//
//  EZSetProgressHUD.h
//  EZSetProgressHUD_Example
//
//  Created by 陈诚 on 2020/1/5.
//  Copyright © 2020 melo30. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EZSetProgressHUD : UIView

//单例
+ (EZSetProgressHUD *)shareInstance;

#pragma mark - 文本
+ (void)show:(NSString *)content;

+ (void)showWithView:(UIView *)view content:(NSString *)content;

- (void)show:(NSString *)content;

- (void)showWithView:(UIView *)view content:(NSString *)content;

#pragma mark - 活动展示器
+ (void)showLoading;

+ (void)showLoadingWithView:(UIView *)view;

- (void)showLoadingWithView:(UIView *)view;

- (void)showLoading;


#pragma mark - 活动展示器加文本(不传文本或者文本传的不是字符串就默认为活动展示器)
+ (void)showLoading:(NSString *)content;

+ (void)showLoadingWithView:(UIView *)view content:(NSString *)content;

- (void)showLoading:(NSString *)content;

- (void)showLoadingWithView:(UIView *)view content:(NSString *)content;

#pragma mark - 隐藏
+ (void)hide;

- (void)hide;

/** 背景视图 */
@property (nonatomic, strong) UIView *backView;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong, nullable) UIActivityIndicatorView *spinner;

@end

NS_ASSUME_NONNULL_END
