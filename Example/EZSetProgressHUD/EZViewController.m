//
//  EZViewController.m
//  EZSetProgressHUD
//
//  Created by melo30 on 01/05/2020.
//  Copyright (c) 2020 melo30. All rights reserved.
//

#import "EZViewController.h"
#import "EZSetProgressHUD.h"
@interface EZViewController ()

@end

@implementation EZViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
#pragma mark - 默认加载在window上面
        #if 0
            //单文本(类方法访问)
            [EZSetProgressHUD show:@"加载中..."];
        #endif

        #if 1
            //单文本(单例对象方法访问)
            [[EZSetProgressHUD shareInstance] show:@"加载中..."];
        #endif
            
        #if 0
            //活动展示器(类方法访问)
            [EZSetProgressHUD showLoading];
        #endif
            
        #if 0
            //活动展示器(单例对象方法访问)
            [[EZSetProgressHUD shareInstance] showLoading];
        #endif
            
        #if 0
            //活动展示器加文本(类方法访问)
            [EZSetProgressHUD showLoading:@"加载中..."];
        #endif
            
        #if 0
            //活动展示器(单例对象方法访问)
            [[EZSetProgressHUD shareInstance] showLoading:@"加载中..."];
        #endif
        
        
#pragma mark - 加载在指定的view上面
        #if 0
            //活动展示器，加载在指定的的view上面(类方法访问)
            [EZSetProgressHUD showLoadingWithView:<#(nonnull UIView *)#>];
        #endif
        
        #if 0
            //活动展示器，加载在指定的的view上面(单例对象方法访问)
            [EZSetProgressHUD shareInstance] showLoadingWithView:<#(nonnull UIView *)#>;
        #endif
        
        #if 0
            //纯文字，加载在指定的的view上面(类方法访问)
            [[EZSetProgressHUD showWithView:<#(nonnull UIView *)#> content:@"加载中"]
        #endif
        
        #if 0
            //纯文字，加载在指定的view上面(单例对象方法访问)
            [[EZSetProgressHUD shareInstance] showWithView:<#(nonnull UIView *)#> content:@""];
        #endif
             
        #if 0
            //活动展示器加文字，加载在指定的view上面(类方法访问)
             [[EZSetProgressHUD showLoadingWithView:<#(nonnull UIView *)#> content:@"加载中"];
        #endif
              
        #if 0
            //活动展示器加文字，加载在指定的view上面(单例对象方法访问)
            [[EZSetProgressHUD shareInstance] showLoadingWithView:<#(nonnull UIView *)#> content:@"加载中"]
        #endif
              
    });
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
