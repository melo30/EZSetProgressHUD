# EZSetProgressHUD

[![CI Status](https://img.shields.io/travis/melo30/EZSetProgressHUD.svg?style=flat)](https://travis-ci.org/melo30/EZSetProgressHUD)
[![Version](https://img.shields.io/cocoapods/v/EZSetProgressHUD.svg?style=flat)](https://cocoapods.org/pods/EZSetProgressHUD)
[![License](https://img.shields.io/cocoapods/l/EZSetProgressHUD.svg?style=flat)](https://cocoapods.org/pods/EZSetProgressHUD)
[![Platform](https://img.shields.io/cocoapods/p/EZSetProgressHUD.svg?style=flat)](https://cocoapods.org/pods/EZSetProgressHUD)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

EZSetProgressHUD is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'EZSetProgressHUD'
```

## Author

melo30, 281083409@qq.com

## License

EZSetProgressHUD is available under the MIT license. See the LICENSE file for more info.
